require "shell"
class Fetch::OS # Call once then store as a constant variablea
  # This grabs the kernal (ie, Darwin => MacOS, Arch => Arch)
  def self.new
    os = Shell.run("uname -s").to_s.chomp # TODO: remove \n via grep awk or something
    case os
    when "Darwin" # Works
      os = "MacOS"
    when "Linux"
      os = "Linux"
    else
      os = "error"
      raise "Unknown Operating System: #{os}..." # TODO: Change to proper exception
    end
    return os # => "Linux"
  end

  def self.distro_pretty
    os = OS_G
    case os
    when "Darwin" # Works
      dist = "MacOS"
    when "Linux"
      # os = "#{Shell.run(grep "NAME" -m 1  /etc/os-release|cut -f2- -d "=")}" # The Bash Way
      dist = File.read_lines("/etc/os-release")[0..3]
      dist = dist[0].to_s
      dist = dist.gsub("NAME\=","")
    else
      os = "error"
      raise "Unknown Distro: #{os}..." # TODO: Change to proper exception
    end
    return dist
  end

  def self.distro
    case OS_G
    when "Linux"
      dist = File.read_lines("/etc/os-release")[0..3]
      dist = dist[0].to_s.gsub("NAME\=","")
    else nil
    end
    case dist
    when "MacOS"
      dist = "MacOS"
    when "Manjaro Linux"
      dist = File.read_lines("/etc/os-release")[0..3]
      dist = dist[1].to_s.gsub("ID=","")
    when "\"Arch Linux\""
      dist = File.read_lines("/etc/os-release")[0..3]
      dist = dist[2].to_s.gsub("ID=","")
    else
      os = "error"
      raise "Unknown Distro: #{dist}..." # TODO: Change to proper exception
    end
    return dist
  end

end
