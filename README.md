# CrFetch

A simple fetch program to print your system details for that sweet /g/ street cred

The idea is for it to work much like NeoFetch

## Installation

``` sh
$ shards && cake # => ./bin/crfetch
```

## Usage

After making with cake run the binary
``` sh
$ ./bin/crfetch
```

You can create an alias for this so it's easier to access

The idea is that it'll work like this to specify what you want to output

``` sh
$ crfetch -c -i -cc
```

## Development

Follow install instructions.

Make sure to run tests:

``` sh
$ crystal spec
```

All tests should pass before developing. If tests do not pass make sure to look
at the log and figure out why, if it's a bug or a feature that you're wanting then
make an issue and if you want to then make a pull request for it.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Konstantine](https://git.materialfuture.net/MaterialFuture) - creator and maintainer

## Notes

I realize this depends a lot on the shell's features IE grep and such, they idea is to wireframe the functions of the application via shell scripts then break out into modular Crystal or C code to make the application leaner and not rely on shell or any dependancies.

Creating flags and such are an optional element that I wish to add but the requires the use of admiral unless there's another way to go about doing that.

General QOL will be having it work on all base platforms and printing out the distro's symbol (ie Arch, FreeBSD, NixOS).
